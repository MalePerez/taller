#include <WiFiEsp.h>
#include <WiFiEspclient.h>
#include <WiFiEspUdp.h>
#include <PubSubClient.h>

IPAddress server(192, 168, 137, 118);
char ssid[] = "macFake";           // network
char pass[] = "12121212";           //  password
int status = WL_IDLE_STATUS;   // the Wifi radio's status



// AT+CIPSTA="192.168.22.3","255.255.255.0","192.168.22.1"

WiFiEspClient espclient;
PubSubClient client(espclient);

void initWiFi() 
{
  Serial1.begin(115200);
  WiFi.init(&Serial1);

  if (WiFi.status() == WL_NO_SHIELD) {
    Serial.println("WiFi shield not present");
    // don't continue
    while (true);
  }
}

void connect()
{
  // attempt to connect to WiFi network
  while ( status != WL_CONNECTED) {
    Serial.print("Attempting to connect to WPA SSID: ");
    Serial.println(ssid);
    // Connect to WPA/WPA2 network
    status = WiFi.begin(ssid, pass);
  }
  // you're connected now, so print out the data
  Serial.println("You're connected to the network");
}

void configurarIP(String local_ip, String subnetmask, String gateway)
{
  Serial.println(".....................................................");
  Serial.println("Iniciando Configuracion como servidor");
  Serial1.println("AT+CIPSTA=\"" + local_ip + "\",\"" + subnetmask +"\",\"" + gateway +"\"\r\n"); //AT+CIPSTA="192.168.1.76","255.255.255.0","192.168.1.73"
  String respuesta = recibir_wifi("OK");
  if (respuesta.lastIndexOf("OK" >= 0)) 
  {
    Serial.println("Configurado");
    
  }
  else
  {
    Serial.println("Error configurando punto de acceso");
  }

}

String recibir_wifi(String delimitador) 
{
  String dato_recibido = "";
  unsigned long espera = 10000 + millis();
  while (espera > millis() && dato_recibido.lastIndexOf(delimitador) < 0) {
    while (Serial1.available()) {
      char c = Serial1.read();
      dato_recibido = dato_recibido + c;
    }
  }
  return dato_recibido;
}

void initMQTT()
{
  /*  connect to MQTT setServer   */
  client.setServer(server, 1883);
  client.setCallback(callback);
}

//print any message received for subscribed topic
void callback(char* topic, byte* payload, unsigned int length) 
{
  String messageReceived = ""; 
  for (int i=0;i<length;i++) {
    messageReceived = messageReceived + (char)payload[i];
  }

  Serial.println(messageReceived);
  if (messageReceived == "1")
  {
    digitalWrite(13, HIGH);
  }
  else if(messageReceived == "0")
  {
    digitalWrite(13, LOW);
  }
  
}
int count = 0;
unsigned long lastSend;

void publicarPrueba(String data) {
  // put your main code here, to run repeatedly:
  if (!client.connected()) 
  {
    reconnect();
  }
  client.loop();
  

  char attributes[100];
  data.toCharArray(attributes, 100 );
  
  long now = millis();

    if ( millis() - lastSend > 1000 ) 
    { 
      client.publish("sensor", attributes);
      client.loop();
      lastSend = millis();
    }

}


void publicarSensores(String presion, String corriente , String cic) {
  // put your main code here, to run repeatedly:

  Serial.println("Enviando datos de sensores");
  if (!client.connected()) 
  {
    reconnect();
  }

  
  Serial.println("if de publicacion");
  publicar("GST/Saltillo/Maquinas/Schwabe1/presion", presion);
  publicar("GST/Saltillo/Maquinas/Schwabe1/corriente", corriente);
  publicar("GST/Saltillo/Maquinas/Schwabe1/ciclos", cic);

}

void publicarInfoCabezal(String tipoCabezal, String rectPines, String rectPlates, String paso, String numCabezal, String lote) {
  // put your main code here, to run repeatedly:
  Serial.println("Enviando informacion cabezal");
  if (!client.connected()) 
  {
    reconnect();
  }
  client.loop();

  publicar("GST/Saltillo/Maquinas/Ring1/TipoCabezal", tipoCabezal);
  publicar("GST/Saltillo/Maquinas/Ring1/RectificacionPines", rectPines);
  publicar("GST/Saltillo/Maquinas/Ring1/RectificacionPlates", rectPlates);
  publicar("GST/Saltillo/Maquinas/Ring1/Paso", paso);
  publicar("GST/Saltillo/Maquinas/Ring1/NumeroCabezal", numCabezal);

  client.loop();

  lastSend = millis();
}

void reconnect() {
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect, just a name to identify the client
    if (client.connect("MQTTPUBLISH")) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      //client.subscribe("led");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void publicar(const char* topic, String mensaje)
{
  char attributes[100];
  mensaje.toCharArray(attributes, 100 );
  client.publish(topic, attributes);
}

