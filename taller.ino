#include <NewPing.h>
#include <DHT11.h>

     #define TRIGGER_PIN 7 // Arduino pin tied to trigger pin on the ultrasonic sensor.    
     #define ECHO_PIN 6    // Arduino pin tied to echo pin on the ultrasonic sensor.
     #define MAX_DISTANCE 100 
     #define led 13
     NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // NewPing setup of pins and maximum distance
 

int pin=4;
DHT11 dht11(pin);
 
     void setup() 
        {
           Serial.begin(9600); // Open serial monitor at 115200 baud to see ping results.
           pinMode(led, OUTPUT);
           initWiFi();
           connect();
           initMQTT();
          
        }
 
     void loop()
       {
           delay(50); 
           unsigned int uS = sonar.ping(); // Send ping, get ping time in microseconds (uS)
           Serial.print("Distancia: ");
           Serial.print(uS / US_ROUNDTRIP_CM); 
           Serial.println("cm");
           delay(500);
           if ( uS / US_ROUNDTRIP_CM > 15){
                 digitalWrite ( led , HIGH) ;
               
           }else{
                 digitalWrite( led , LOW) ;
                   
            delay (500) ;                  // Para limitar el número de mediciones
        }
        int err;
       float temp, hum;
       if((err = dht11.read(hum, temp)) == 0)    // Si devuelve 0 es que ha leido bien
          {
             Serial.print("Temperatura: ");
             Serial.print(temp);
             Serial.print(" Humedad: ");
             Serial.print(hum);
             Serial.println();
             publicarSensores((String)temp,(String)hum,(String)(uS / US_ROUNDTRIP_CM));
          }
       else
          {
             Serial.println();
             Serial.print("Error Num :");
             Serial.print(err);
             Serial.println();
          }
       delay(1000);          
       }
